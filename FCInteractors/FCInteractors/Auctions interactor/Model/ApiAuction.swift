//
//  Auction.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

struct ApiAuction: Equatable {
    let id: Int
    let amount: Money
    let rate: Double
    let term: Int
    let title: String
    let closeDate: Date
    let riskBand: RiskBand
}

extension ApiAuction {
    init?(jsonDictionary: JSONDictionary, dateFormatter: DateFormatter) {
        guard let amount = jsonDictionary["amount_cents"] as? Int,
            let closeTimeString = jsonDictionary["close_time"] as? String,
            let closeDate = dateFormatter.date(from: closeTimeString),
            let id = jsonDictionary["id"] as? Int,
            let rate = jsonDictionary["rate"] as? Double,
            let title = jsonDictionary["title"] as? String,
            let term = jsonDictionary["term"] as? Int,
            let riskBandString = jsonDictionary["risk_band"] as? String,
            let riskBand = RiskBand(riskBand: riskBandString) else {
                return nil
        }
        
        self.init(id: id,
                  amount: Money(cents: amount),
                  rate: rate,
                  term: term,
                  title: title,
                  closeDate: closeDate,
                  riskBand: riskBand)
    }
}
