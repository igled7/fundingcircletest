//
//  Money.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

/// This struct represents money.
/// In the future we could add here the currency.
public struct Money: Equatable {
    public let cents: Int
    public init(cents: Int) {
        self.cents = cents
    }
}
