//
//  Auction.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

public struct Auction: Equatable {
    public let title: String
    public let rate: Double
    public let estimatedReturn: Money
    public let riskBand: RiskBand
    public let closeDate: Date
    
    public init(title: String, rate: Double, estimatedReturn: Money, riskBand: RiskBand, closeDate: Date) {
        self.title = title
        self.rate = rate
        self.estimatedReturn = estimatedReturn
        self.riskBand = riskBand
        self.closeDate = closeDate
    }
}
