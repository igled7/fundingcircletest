//
//  RiskBand.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

public enum RiskBand: String {
    case aPlus = "A+"
    case a = "A"
    case b = "B"
    case c = "C"
    case d = "D"
}

extension RiskBand {
    
    var estimatedBadDebt: Double {
        switch self {
        case .aPlus:
            return 0.01
        case .a:
            return 0.02
        case .b:
            return 0.03
        case .c:
            return 0.04
        case .d:
            return 0.05
        }
    }
    
    init?(riskBand: String) {
        switch riskBand {
        case "A+":
            self = .aPlus
        case "A":
            self = .a
        case "B", "B-":
            self = .b
        case "C", "C-":
            self = .c
        case "D", "D-":
            self = .d
        default:
            return nil
        }
    }
}
