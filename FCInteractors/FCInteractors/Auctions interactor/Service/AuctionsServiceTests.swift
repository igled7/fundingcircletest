//
//  AuctionsServiceTests.swift
//  FCInteractorsTests
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import XCTest
@testable import FCInteractors

class AuctionsServiceTests: XCTestCase {
    
    var mockApiClient: MockApiClient!
    var sut: AuctionsService!

    override func setUp() {
        super.setUp()
        mockApiClient = MockApiClient()
        sut = AuctionsService(apiClient: mockApiClient)
    }
    
    func test_init_auctionsService() {
        XCTAssertTrue(sut.apiClient === mockApiClient)
    }
    
    func test_loadAuctions_buildsValidAuctionsResource() {
        
        sut.loadAuctions { _ in }
        
        XCTAssertEqual(mockApiClient.latestResource?.path, .auctionsPath)
        XCTAssertEqual(mockApiClient.latestResource?.httpMethod, HttpMethod.get)
        XCTAssertTrue(mockApiClient?.latestResource?.parameters.count == 0)
    }
    
    func test_loadAuctions_returnsError_whenApiClientReturnsAnError() {
        
        let completionExpectation = expectation(description: "completion block should be called")

        sut.loadAuctions { result in
            switch result {
            case .failure(let error as Error):
                XCTAssertEqual(error, Error.testError)
            default:
                XCTFail()
            }
            completionExpectation.fulfill()
        }
        
        mockApiClient.latestCompletion?(Result<ApiResponse>.failure(Error.testError))
        
        waitForExpectations(timeout: 0, handler: nil)
    }
    
    func test_loadAuctions_returnsError_whenApiClientReturnsUnsuccessfulApiResponse() {
        
        let completionExpectation = expectation(description: "completion block should be called")
        let apiResponse = ApiResponse(responseCode: 500, data: nil)
        
        sut.loadAuctions { result in
            switch result {
            case .failure(let error as ServiceError):
                XCTAssertEqual(error, ServiceError.genericError)
            default:
                XCTFail()
            }
            completionExpectation.fulfill()
        }
        
        mockApiClient.latestCompletion?(Result<ApiResponse>.success(apiResponse))
        
        waitForExpectations(timeout: 0, handler: nil)
    }
    
    func test_loadAuctions_returnsAuctions_whenApiClientReturnsSuccessfulApiResponse() {

        let completionExpectation = expectation(description: "completion block should be called")
        let auctionsData = DataFileLoader.load(file: "AuctionsJsonResponse")
        let apiResponse = ApiResponse(responseCode: 200, data: auctionsData)
        let expectedAuction = ApiAuction.makeTestAuction()
        
        sut.loadAuctions { result in
            switch result {
            case .success(let auctions):
                XCTAssertFalse(auctions.isEmpty)
                XCTAssertEqual(auctions[0], expectedAuction)
            default:
                XCTFail()
            }
            completionExpectation.fulfill()
        }

        mockApiClient.latestCompletion?(Result.success(apiResponse))

        waitForExpectations(timeout: 0, handler: nil)
    }
}

private extension ApiAuction {
    static func makeTestAuction() -> ApiAuction {
        let dateFormatter = DateFormatter.iso8601

        return  ApiAuction(id: 8,
                        amount: Money(cents: 10),
                        rate: 0.07,
                        term: 36,
                        title: "Daniel",
                        closeDate: dateFormatter.date(from: "2018-09-21T03:24:53.195Z")!,
                        riskBand: .aPlus)
    }
}

private extension String {
    static let auctionsPath = "auctions"
}

private enum Error: Swift.Error {
    case testError
}

