//
//  MockAuctionsService.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation
@testable import FCInteractors

final class MockAuctionsService: AuctionsServiceProtocol {
    
    var latestCompletion: ((Result<[ApiAuction]>) -> ())?

    func loadAuctions(completion: @escaping (Result<[ApiAuction]>) -> ()) {
        latestCompletion = completion
    }
}
