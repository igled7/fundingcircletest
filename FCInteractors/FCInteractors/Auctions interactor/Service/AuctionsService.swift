//
//  AuctionsService.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

protocol AuctionsServiceProtocol: AnyObject {
    func loadAuctions(completion: @escaping (Result<[ApiAuction]>) -> ())
}

final class AuctionsService: AuctionsServiceProtocol {
    
    let apiClient: ApiClientProtocol
    init(apiClient: ApiClientProtocol) {
        self.apiClient = apiClient
    }
    
    func loadAuctions(completion: @escaping (Result<[ApiAuction]>) -> ()) {
        let resource = Resource(path: "auctions")
        apiClient.load(resource) { result in
            completion(result.parseJson(auctionsParser))
        }
    }
}

private func auctionsParser(jsonDictionary: JSONDictionary) -> [ApiAuction] {
    guard let auctions = jsonDictionary["items"] as? [JSONDictionary] else {
        return []
    }
    return auctions.compactMap { (json) -> ApiAuction? in
        return ApiAuction(jsonDictionary: json, dateFormatter: DateFormatter.iso8601)
    }
}
 

