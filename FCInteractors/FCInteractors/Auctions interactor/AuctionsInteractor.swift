//
//  AuctionsInteractor.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

public protocol AuctionsInteractorProtocol: AnyObject {
    func loadAuctions(completion: @escaping (Result<[Auction]>) -> ())
}

final public class AuctionsInteractor: AuctionsInteractorProtocol {
    
    let auctionsService: AuctionsServiceProtocol
    let dispatcher: Dispatcher
    init(auctionsService: AuctionsServiceProtocol, dispatcher: Dispatcher = Dispatcher()) {
        self.auctionsService = auctionsService
        self.dispatcher = dispatcher
    }
    
    public func loadAuctions(completion: @escaping (Result<[Auction]>) -> ()) {
        auctionsService.loadAuctions { [weak self] (serviceResult) in
            let interactorResult = serviceResult.map { apiAuctions -> [Auction] in
                return apiAuctions.map(Auction.init)
            }
            self?.dispatcher.dispatchOnMainThread {
                completion(interactorResult)
            }
        }
    }
}

private extension Auction {
    init(apiAuction: ApiAuction) {
        let minimunBidCents: Double = 2000
        let fee = 0.01
        let estimatedBadDebt = apiAuction.riskBand.estimatedBadDebt
        let estimatedAmount = (Double(1) + apiAuction.rate - estimatedBadDebt - fee) * minimunBidCents
        title = apiAuction.title
        riskBand = apiAuction.riskBand
        closeDate = apiAuction.closeDate
        rate = apiAuction.rate
        estimatedReturn = Money(cents: Int(estimatedAmount))
    }
}

