//
//  SynchronousDispatcher.swift
//  FCInteractorsTests
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation
@testable import FCInteractors

final class SynchronousDispatcher: Dispatcher {
    override func dispatchOnMainThread(_ task: @escaping () -> ()) {
        task()
    }
}
