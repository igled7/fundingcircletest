//
//  AuctionsInteractorTests.swift
//  FCInteractorsTests
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import XCTest
@testable import FCInteractors

class AuctionsInteractorTests: XCTestCase {
    
    var sut: AuctionsInteractor!
    var mockAuctionsService: MockAuctionsService!
    var synchronousDispatcher: SynchronousDispatcher!

    override func setUp() {
        super.setUp()
        mockAuctionsService = MockAuctionsService()
        synchronousDispatcher = SynchronousDispatcher()
        sut = AuctionsInteractor(auctionsService: mockAuctionsService, dispatcher: synchronousDispatcher)
    }
    
    func test_init() {
        XCTAssertTrue(sut.auctionsService === mockAuctionsService)
    }
    
    func test_errorReturned_whenServiceReturnsError() {
        
        let serviceError = ServiceError.genericError
        let completionExpectation = expectation(description: "completion block should be called")
                
        sut.loadAuctions { result in
            switch result {
            case .failure(let error as ServiceError):
                XCTAssertEqual(error, serviceError)
            default:
                XCTFail()
            }
            completionExpectation.fulfill()
        }
        
        mockAuctionsService.latestCompletion?(Result<[ApiAuction]>.failure(serviceError))

        waitForExpectations(timeout: 0, handler: nil)
    }
    
    func test_auctionsReturned_whenServiceReturnsApiAuctions() {
        
        func assertAuction(_ auction: Auction, isEqualTo apiAuction: ApiAuction, expectedReturn: Money) {
            XCTAssertEqual(auction.closeDate, apiAuction.closeDate)
            XCTAssertEqual(auction.title, apiAuction.title)
            XCTAssertEqual(auction.riskBand, apiAuction.riskBand)
            XCTAssertEqual(auction.rate, apiAuction.rate)
            XCTAssertEqual(auction.estimatedReturn, expectedReturn)
        }
        
        let apiAuction1 = ApiAuction(id: 1,
                                     amount: Money(cents: 10),
                                     rate: 1,
                                     term: 1,
                                     title: "apiAuction1Title",
                                     closeDate: Date(timeIntervalSince1970: 0),
                                     riskBand: .aPlus)
    
        let apiAuction2 = ApiAuction(id: 2,
                                     amount: Money(cents: 10),
                                     rate: 10,
                                     term: 2,
                                     title: "apiAuction2Title",
                                     closeDate: Date(timeIntervalSince1970: 10),
                                     riskBand: RiskBand.a)
    
        let completionExpectation = expectation(description: "completion block should be called")
        
        sut.loadAuctions { result in
            switch result {
            case .success(let auctions):
                XCTAssertTrue(auctions.count == 2)
                assertAuction(auctions[0], isEqualTo: apiAuction1, expectedReturn: Money(cents: 3960))
                assertAuction(auctions[1], isEqualTo: apiAuction2, expectedReturn: Money(cents: 21940))
            default:
                XCTFail()
            }
            completionExpectation.fulfill()
        }
        
        mockAuctionsService.latestCompletion?(Result<[ApiAuction]>.success([apiAuction1, apiAuction2]))
        
        waitForExpectations(timeout: 0, handler: nil)
    }
    
    func test_completionCalledOnMainThread() {
        
        sut = AuctionsInteractor(auctionsService: mockAuctionsService)
        let serviceError = ServiceError.genericError
        let completionExpectation = expectation(description: "completion block should be called")
        
        DispatchQueue.global(qos: .background).async {
            self.sut.loadAuctions { result in
                XCTAssertTrue(Thread.isMainThread)
                completionExpectation.fulfill()
            }
            self.mockAuctionsService.latestCompletion?(Result<[ApiAuction]>.failure(serviceError))
        }
        
        waitForExpectations(timeout: 1, handler: nil)
    }
}
