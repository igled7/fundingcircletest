//
//  MockURLSession.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation
@testable import FCInteractors

class MockURLSession: URLSessionProtocol {
    
    var urlSessionDataTask: MockURLSessionDataTask
    init() {
        self.urlSessionDataTask = MockURLSessionDataTask()
    }
    
    var latestDataTaskURLRequest: URLRequest?
    var latestDataTaskCompletionHandler: ((Data?, URLResponse?, Error?) -> Void)?
    
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        latestDataTaskURLRequest = request
        latestDataTaskCompletionHandler = completionHandler
        return urlSessionDataTask
    }
}
