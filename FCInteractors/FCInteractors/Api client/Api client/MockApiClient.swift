//
//  MockApiClient.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation
@testable import FCInteractors


class MockApiClient: ApiClientProtocol {

    var latestCompletion: ((Result<ApiResponse>) -> ())?
    var latestResource: Resource?
    
    func load(_ resource: Resource, completion: @escaping (Result<ApiResponse>) -> ()) {
        latestResource = resource
        latestCompletion = completion
    }
}
