//
//  MockURLSessionDataTask.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

class MockURLSessionDataTask: URLSessionDataTask {

    var resumeCalled = false
    
    override func resume() {
        resumeCalled = true
    }
}
