//
//  ApiClientTests.swift
//  FCInteractorsTests
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import XCTest
@testable import FCInteractors

class ApiClientTests: XCTestCase {
    
    var mockURLSession: MockURLSession!
    var sut: ApiClient!
    
    override func setUp() {
        super.setUp()
        mockURLSession = MockURLSession()
        sut = ApiClient(basePath: .testBasePath, urlSession: mockURLSession)
    }
    
    func test_initWithBasePath() {
        let sut = ApiClient(basePath: .testBasePath)
        XCTAssertEqual(sut.basePath, .testBasePath)
    }
}

// MARK: Generic tests
extension ApiClientTests {
    
    func test_dataTaskIsResumed() {
        
        let resource = Resource(path: .testPath)
        let mockURLSessionDataTask = mockURLSession.urlSessionDataTask
        sut.load(resource) { _ in }
        
        XCTAssertTrue(mockURLSessionDataTask.resumeCalled)
    }
}

// MARK: Get request tests
extension ApiClientTests {
    
    func test_getRequest_usesValidURLRequest() {
        
        let resource = Resource(path: .testPath)
        let expectedURLRequest = URLRequest(basePath: .testBasePath, path: .testPath)
        
        sut.load(resource) { _ in }
        
        XCTAssertEqual(mockURLSession.latestDataTaskURLRequest, expectedURLRequest)
    }
    
    func test_getRequestWithParameters_usesValidURLRequest() {
        
        let resource = Resource(path: .testPath, parameters: .testParameters)
        let expectedURLRequest = URLRequest(basePath: .testBasePath, path: .testPath, parameters: .testParameters)
        
        sut.load(resource) { _ in }
        
        XCTAssertEqual(mockURLSession.latestDataTaskURLRequest, expectedURLRequest)
    }
}

// MARK: Post request tests
extension ApiClientTests {
    
    func test_postRequest_usesValidURLRequest() {
        
        let resource = Resource(path: .testPath, httpMethod: .post(.testData))
        let expectedURLRequest = URLRequest(basePath: .testBasePath, path: .testPath, method: "POST", data: .testData)
        
        sut.load(resource) { _ in }
        
        XCTAssertEqual(mockURLSession.latestDataTaskURLRequest, expectedURLRequest)
    }
}

// MARK: Completion tests
extension ApiClientTests {
    
    func test_request_returnsError_whenDataTaskCompletes_withError() {
        
        let resource = Resource(path: .testPath)
        let expectedError = Error.testError
        let completionExpectation = expectation(description: "completion block should be called")
        
        sut.load(resource) { result in
            switch result {
            case .failure(let error as Error):
                XCTAssertEqual(error, expectedError)
            default:
                XCTFail()
            }
            completionExpectation.fulfill()
        }
        mockURLSession.latestDataTaskCompletionHandler?(nil, nil, expectedError)
        waitForExpectations(timeout: 0, handler: nil)
    }
    
    func test_request_returnsError_whenDataTaskCompletes_withInvalidURLResponse() {
        
        let resource = Resource(path: .testPath)
        let completionExpectation = expectation(description: "completion block should be called")
        
        sut.load(resource) { result in
            switch result {
            case .failure(let error as ApiError):
                XCTAssertEqual(error, ApiError.unknownError)
            default:
                XCTFail()
            }
            completionExpectation.fulfill()
        }
        
        mockURLSession.latestDataTaskCompletionHandler?(nil, URLResponse(), nil)
        waitForExpectations(timeout: 0, handler: nil)
    }
    
    func test_request_ApiResponse_whenDataTaskCompletes_withHTTPResponse() {
        
        let resource = Resource(path: .testPath)
        let completionExpectation = expectation(description: "completion block should be called")
        let expectedData = Data.testData
        let httpURLResponse = HTTPURLResponse(statusCode: 123)
        
        sut.load(resource) { result in
            switch result {
            case .success(let apiResponse):
                XCTAssertEqual(apiResponse.data, expectedData)
                XCTAssertEqual(apiResponse.responseCode, httpURLResponse.statusCode)
            default:
                XCTFail()
            }
            completionExpectation.fulfill()
        }
        mockURLSession.latestDataTaskCompletionHandler?(expectedData, httpURLResponse, nil)
        waitForExpectations(timeout: 0, handler: nil)
    }
}

private extension HTTPURLResponse {
    convenience init(statusCode: Int) {
        self.init(url: URL(string: "anything")!, statusCode: statusCode, httpVersion: nil, headerFields: nil)!
    }
}

private extension URLRequest {
    
    init(basePath: String, path: String, parameters: [String: String] = [:], method: String = "GET", data: Data? = nil) {
        let url = URL(string: path, relativeTo: URL(string: basePath)!)!
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)!
        if parameters.count > 0 {
            urlComponents.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        }
        self.init(url: urlComponents.url!)
        httpMethod = method
        if let data = data {
            httpBody = data
        }
    }
}

private enum Error: Swift.Error {
    case testError
}

private extension Data {
    static let testData = "testData".data(using: .utf8)!
}

private extension String {
    static let testPath = "test"
    static let testBasePath = "http://www.test.com"
}

private extension Dictionary where Key == String, Value == String {
    static let testParameters = ["token" : "1234"]
}
