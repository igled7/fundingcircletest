//
//  ApiClient.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

public enum ApiError: Error {
    case unknownError
}

protocol ApiClientProtocol: AnyObject {
    func load(_ resource: Resource, completion: @escaping (Result<ApiResponse>) -> ())
}

final class ApiClient: ApiClientProtocol {
    
    let basePath: String
    let urlSession: URLSessionProtocol
    
    init(basePath: String, urlSession: URLSessionProtocol = URLSession.shared) {
        self.basePath = basePath
        self.urlSession = urlSession
    }
    
    func load(_ resource: Resource, completion: @escaping (Result<ApiResponse>) -> ()) {
        
        let resourceURLRequest = URLRequest(basePath: basePath, resource: resource)
        
        let dataDask = urlSession.dataTask(with: resourceURLRequest) { (data, response, error) in
            
            let result: Result<ApiResponse>
            
            if let error = error {
                result = Result<ApiResponse>.failure(error)
                
            } else if let response = response as? HTTPURLResponse {
                let apiResponse = ApiResponse(responseCode: response.statusCode, data: data)
                result = Result<ApiResponse>.success(apiResponse)
                
            } else {
                result = Result<ApiResponse>.failure(ApiError.unknownError)
            }
            completion(result)
        }
        
        dataDask.resume()
    }
}

extension URLSession: URLSessionProtocol {}
