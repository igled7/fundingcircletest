//
//  URLRequest+Resource.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

extension URLRequest {
    init(basePath: String, resource: Resource) {
        
        guard let baseUrl = URL(string: basePath),
            let url = URL(string: resource.path, relativeTo: baseUrl),
            var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
                fatalError("Please, check that your base api path and resource are valid")
        }
        
        if !resource.parameters.isEmpty {
            urlComponents.queryItems = resource.parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        }
        
        guard let resourceUrl = urlComponents.url else {
            fatalError("Please, make sure your resource url parameters are valid")
        }
        
        self.init(url: resourceUrl)
        httpMethod = resource.httpMethod.method
        
        if case let .post(body) = resource.httpMethod {
            httpBody = body
        }
    }
}
