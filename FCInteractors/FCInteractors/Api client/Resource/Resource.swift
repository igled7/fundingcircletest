//
//  Resource.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

enum HttpMethod: Equatable {
    case get
    case post(Data)
}

extension HttpMethod {
    var method: String {
        switch self {
        case .get:
            return "GET"
        case .post:
            return "POST"
        }
    }
}

struct Resource {
    let path: String
    let httpMethod: HttpMethod
    let parameters: [String: String]
    
    init(path: String, httpMethod: HttpMethod = .get, parameters: [String: String] = [:]) {
        self.path = path
        self.httpMethod = httpMethod
        self.parameters = parameters
    }
}
