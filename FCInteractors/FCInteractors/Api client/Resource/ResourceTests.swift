//
//  ResourceTests.swift
//  FCInteractorsTests
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import XCTest
@testable import FCInteractors

class ResourceTests: XCTestCase {
    
    func test_getRequest() {
        
        let sut = Resource(path: .testPath)
        
        XCTAssertEqual(sut.path, .testPath)
        XCTAssertEqual(sut.httpMethod, HttpMethod.get)
        XCTAssertEqual(sut.parameters, [:])
    }
    
    func test_init_getRequest_withParameters() {
        
        let sut = Resource(path: .testPath, parameters: .testParameters)
        
        XCTAssertEqual(sut.path, .testPath)
        XCTAssertEqual(sut.httpMethod, HttpMethod.get)
        XCTAssertEqual(sut.parameters, .testParameters)
    }
    
    func test_postRequest() {
        
        let sut = Resource(path: .testPath, httpMethod: HttpMethod.post(.testData))
        
        XCTAssertEqual(sut.path, .testPath)
        XCTAssertEqual(sut.httpMethod, HttpMethod.post(.testData))
    }
    
    func test_postRequest_withParameters() {
        
        let sut = Resource(path: .testPath, httpMethod: HttpMethod.post(.testData), parameters: .testParameters)
        
        XCTAssertEqual(sut.path, .testPath)
        XCTAssertEqual(sut.httpMethod, HttpMethod.post(.testData))
        XCTAssertEqual(sut.parameters, .testParameters)
    }
}

private extension String {
    static let testPath = "test"
}

private extension Data {
    static let testData = "testData".data(using: .utf8)!
}

private extension Dictionary where Key == String, Value == String {
    static let testParameters = ["token" : "1234"]
}
