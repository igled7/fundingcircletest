//
//  InteractorsTests.swift
//  FCInteractorsTests
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import XCTest
@testable import FCInteractors

class InteractorsTests: XCTestCase {
    
    var sut: InteractorsFactory!
    
    override func setUp() {
        super.setUp()
        sut = InteractorsFactory(basePath: "https://www.test.com")
    }

    func test_init() {
        let testBasePath = "https://www.test.com"
        let sut = InteractorsFactory(basePath: testBasePath)
        XCTAssertEqual(sut.apiClient.basePath, testBasePath)
    }
    
    func test_canCreate_auctionsInteractor() {
        let auctionsInteractor = sut.makeAuctionsInteractor()
        XCTAssertNotNil(auctionsInteractor)
    }
}
