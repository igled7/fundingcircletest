//
//  Interactors.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

final public class InteractorsFactory {
    
    let apiClient: ApiClient
    public init(basePath: String) {
        apiClient = ApiClient(basePath: basePath)
    }
    
    public func makeAuctionsInteractor() -> AuctionsInteractor {
        return AuctionsInteractor(auctionsService: AuctionsService(apiClient: apiClient))
    }
}
