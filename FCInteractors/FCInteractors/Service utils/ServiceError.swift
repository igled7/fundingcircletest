//
//  ServiceError.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

public enum ServiceError: Error {
    case genericError
}

