//
//  Result+Parser.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

public enum ParserError: Error {
    case genericError
}

extension Result where T == ApiResponse {
    
    func parseJson<A>(_ parser: (JSONDictionary) throws -> A) rethrows -> Result<A> {

        let result: Result<A>
        switch self {
        case .success(let apiResponse):
            if let data = apiResponse.data, 200..<300 ~= apiResponse.responseCode {
                result = data.parse(parser)
            } else {
                result = Result<A>.failure(ServiceError.genericError)
            }
        case .failure(let error):
            result = Result<A>.failure(error)
        }
        return result
    }
}

private extension Data {
    func parse<A>(_ parser: (JSONDictionary) throws -> A) -> Result<A> {
        let result: Result<A>
        
        let jsonObject = try? JSONSerialization.jsonObject(with: self, options: [])
        
        if let jsonDictionary = jsonObject as? JSONDictionary,
            let parsedData = try? parser(jsonDictionary)   {
            result = Result<A>.success(parsedData)
        } else {
            result = Result<A>.failure(ParserError.genericError)
        }
        return result
    }
}

