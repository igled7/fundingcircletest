//
//  DataFileLoader.swift
//  FCInteractorsTests
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

final class DataFileLoader {
    
    static func load(file: String) -> Data {
        let bundle = Bundle(for: DataFileLoader.self)
        guard let resourcePath = bundle.path(forResource: file, ofType: "json"),
        let data = try? Data(contentsOf: URL(fileURLWithPath: resourcePath)) else {
            fatalError("Make sure you have a valid resource")
        }
        return data
    }
}
