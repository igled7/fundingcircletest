//
//  Dispatcher.swift
//  FCInteractors
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

class Dispatcher {
    func dispatchOnMainThread(_ task: @escaping ()-> ()) {
        DispatchQueue.main.async(execute: task)
    }
}
