//
//  Result.swift
//  FCInteractors
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

public enum Result<T> {
    case success(T)
    case failure(Error)
}

extension Result {
    public func map<B>(_ transform: (T) throws -> B) rethrows -> Result<B> {
        switch self {
        case .failure(let error):
            return Result<B>.failure(error)
        case .success(let value):
            return Result<B>.success(try transform(value))
        }
    }
}
