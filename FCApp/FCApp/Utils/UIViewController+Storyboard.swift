//
//  UIViewController+Storyboard.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit

protocol StoryBoardLoadableViewController: AnyObject {}

extension StoryBoardLoadableViewController where Self: UIViewController {
    
    static var storyBoardName: String {
        return String(describing: self)
    }
    
    static func instantiateViewControllerFromStoryBoard() -> Self {
        
        let storyBoard = UIStoryboard(name: self.storyBoardName, bundle: Bundle.main)
        
        guard let viewController = storyBoard.instantiateInitialViewController() as? Self else {
            fatalError("Could not instantiate view controller from storyboard with name: \(self.storyBoardName)")
        }
        
        return viewController
    }
}
