//
//  AuctionDetailsPresenter.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation
import FCInteractors

struct AuctionDetailsViewModel {
    let title: String
    let estimatedReturn: String
    let rate: String
    let rating: String
    let closeDate: String
}

protocol AuctionDetailsView: AnyObject {
    func display(viewModel: AuctionDetailsViewModel)
}

final class AuctionDetailsPresenter {

    weak var auctionDetailsView: AuctionDetailsView?
    
    let auction: Auction
    init(auction: Auction) {
        self.auction = auction
    }
    
    func startPresenting() {
        let closeDate = Formatter.dateFormatter.string(from: auction.closeDate)
        let rate = Formatter.percentFormatter.string(for: auction.rate) ?? "--"
        let rating = auction.riskBand.rawValue
        let title = auction.title
        let estimatedReturn = Formatter.poundFormatter.string(for: Double(auction.estimatedReturn.cents) / Double(100)) ?? "--"
        let viewModel = AuctionDetailsViewModel(title: title,
                                       estimatedReturn: estimatedReturn,
                                       rate: rate,
                                       rating: rating,
                                       closeDate: closeDate)
        auctionDetailsView?.display(viewModel: viewModel)
    }
}

