//
//  AuctionDetailsViewController.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit

final class AuctionDetailsViewController: UIViewController, StoryBoardLoadableViewController {
    
    var presenter: AuctionDetailsPresenter!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var estimatedReturnLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var closeDateLabel: UILabel!
    
    
    func initialize(withPresenter presenter: AuctionDetailsPresenter) {
        self.presenter = presenter
        presenter.auctionDetailsView = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Auction Details"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.startPresenting()
    }
}

// MARK: AuctionDetailsView
extension AuctionDetailsViewController: AuctionDetailsView {
    
    func display(viewModel: AuctionDetailsViewModel) {
        titleLabel.text = viewModel.title
        estimatedReturnLabel.text = "Estimated return: \(viewModel.estimatedReturn)"
        rateLabel.text = "Average rate: \(viewModel.rate)"
        ratingLabel.text = "Risk band: \(viewModel.rating)"
        closeDateLabel.text = "Close date: \(viewModel.closeDate)"
    }
}
