//
//  AppCoordinator.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit
import FCInteractors

final class AppCoordinator {
    
    private let window: UIWindow
    private let interactorsFactory: InteractorsFactory
    private let appCoordinatorFactory: AppCoordinatorFactory
    private lazy var navigationController = UINavigationController()
    
    init(window: UIWindow, interactorsFactory: InteractorsFactory) {
        self.window = window
        self.interactorsFactory = interactorsFactory
        self.appCoordinatorFactory = AppCoordinatorFactory(interactorsFactory: interactorsFactory)
    }
    
    func start() {
        let rootViewController = appCoordinatorFactory.makeAuctionListViewController(with: self)
        navigationController.pushViewController(rootViewController, animated: false)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}

// MARK: AuctionsListRoutingDelegate
extension AppCoordinator: AuctionsListRoutingDelegate {
    func auctionsViewPresenter(_ sender: AuctionsListPresenter, presentAuctionDetails auction: Auction) {
        let detailsViewController = appCoordinatorFactory.makeAuctionDetailsViewController(withAuction: auction)
        navigationController.pushViewController(detailsViewController, animated: true)
    }
}

