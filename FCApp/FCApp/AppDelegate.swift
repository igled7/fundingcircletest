//
//  AppDelegate.swift
//  FCApp
//
//  Created by Daniel on 15/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit
import FCInteractors

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    private lazy var interactorsFactory = InteractorsFactory(basePath: "https://fc-ios-test.herokuapp.com")
    private lazy var appCoordinator = AppCoordinator(window: window!, interactorsFactory: interactorsFactory)
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow()
        appCoordinator.start()
        return true
    }
}

