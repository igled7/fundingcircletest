//
//  MockAuctionsInteractor.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation
import FCInteractors

class MockAuctionsInteractor: AuctionsInteractorProtocol {
    
    var loadAuctionsCalled = false
    var latestLoadAuctionsCompletion: ((Result<[Auction]>) -> ())?
    
    func loadAuctions(completion: @escaping (Result<[Auction]>) -> ()) {
        loadAuctionsCalled = true
        latestLoadAuctionsCompletion = completion
    }
}
