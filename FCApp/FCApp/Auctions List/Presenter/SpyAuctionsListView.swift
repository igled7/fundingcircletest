//
//  SpyAuctionsListView.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation
@testable import FCApp

class SpyAuctionsListView: AuctionsListView {
    
    var displayLoadingCalled = false
    func displayLoading() {
        displayLoadingCalled = true
    }
    
    var stopLoadingCalled = false
    func stopLoading() {
        stopLoadingCalled = true
    }
    
    var displayErrorTitle = ""
    var displayErrorDescription = ""
    var displayErrorCalled = false
    func displayError(title: String, description: String) {
        displayLoadingCalled = true
        displayErrorTitle = title
        displayErrorDescription = description
    }
    
    var displayAuctionsCalled = false
    var displayAuctionsArgument: [AuctionViewModel] = []
    func displayAuctions(_ auctions: [AuctionViewModel]) {
        displayAuctionsCalled = true
        displayAuctionsArgument = auctions
    }
}
