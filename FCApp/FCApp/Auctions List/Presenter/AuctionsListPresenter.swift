//
//  AuctionsListPresenter.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation
import FCInteractors

protocol AuctionsListView: AnyObject {
    func displayLoading()
    func stopLoading()
    func displayError(title: String, description: String)
    func displayAuctions(_ auctions: [AuctionViewModel])
}

protocol AuctionsListRoutingDelegate: AnyObject {
    func auctionsViewPresenter(_ sender: AuctionsListPresenter, presentAuctionDetails auction: Auction)
}

struct AuctionViewModel {
    let title: String
    let rate: String
    let rating: String
    let closeDate: String
    fileprivate let auction: Auction
    
    fileprivate init(title: String,
                     rate: String,
                     rating: String,
                     closeDate: String,
                     auction: Auction) {
        self.title = title
        self.rate = rate
        self.rating = rating
        self.closeDate = closeDate
        self.auction = auction
    }
}

class AuctionsListPresenter {
    
    weak var auctionsListView: AuctionsListView?
    weak var routingDelegate: AuctionsListRoutingDelegate?

    let auctionsInteractor: AuctionsInteractorProtocol
    init(auctionsInteractor: AuctionsInteractorProtocol) {
        self.auctionsInteractor = auctionsInteractor
    }
    
    func loadAuctions() {
        auctionsListView?.displayLoading()
        auctionsInteractor.loadAuctions { [weak self] auctionsResult in
            guard let strongSelf = self else { return }
            strongSelf.auctionsListView?.stopLoading()
            strongSelf.present(auctionsResult)
        }
    }
    
    func presentDetails(for viewModel: AuctionViewModel) {
        routingDelegate?.auctionsViewPresenter(self, presentAuctionDetails: viewModel.auction)
    }
    
    private func present(_ auctionsResult: Result<[Auction]>) {
     
        switch auctionsResult {
        case .success(let auctions):
            present(auctions)
        case .failure:
            auctionsListView?.displayError(title: "Whoops - Something went wrong",
                                           description: "Please check your internet connection")
        }
    }
    
    private func present(_ auctions: [Auction]) {
        
        let viewModels = auctions.map { (auction) -> AuctionViewModel in
            let closeDate = Formatter.dateFormatter.string(from: auction.closeDate)
            let rate = Formatter.percentFormatter.string(for: auction.rate) ?? "--"
            let rating = auction.riskBand.rawValue
            let title = auction.title
            return AuctionViewModel(title: title, rate: rate, rating: rating, closeDate: closeDate, auction: auction)
        }
        
        auctionsListView?.displayAuctions(viewModels)
    }
}

