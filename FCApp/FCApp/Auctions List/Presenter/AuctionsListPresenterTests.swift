//
//  AuctionsListPresenterTests.swift
//  FCAppTests
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import XCTest
@testable import FCApp
import FCInteractors

class AuctionsListPresenterTests: XCTestCase {
    
    var sut: AuctionsListPresenter!
    var mockAuctionsInteractor: MockAuctionsInteractor!
    var spyView: SpyAuctionsListView!
    
    override func setUp() {
        super.setUp()
        mockAuctionsInteractor = MockAuctionsInteractor()
        spyView = SpyAuctionsListView()
        sut = AuctionsListPresenter(auctionsInteractor: mockAuctionsInteractor)
        sut.auctionsListView = spyView
    }
    
    func test_init() {
        XCTAssertTrue(sut.auctionsInteractor === mockAuctionsInteractor)
    }
    
    func test_loadAuctions_usesInteractor() {
        sut.loadAuctions()
        XCTAssertTrue(mockAuctionsInteractor.loadAuctionsCalled)
    }
    
    func test_loadAuctions_invokesDisplayLoadingOnView() {
        
        let spyView = SpyAuctionsListView()
        sut.auctionsListView = spyView
        
        sut.loadAuctions()
        
        XCTAssertTrue(spyView.displayLoadingCalled)
    }
    
    func test_loadAuctions_invokesStopLoadingOnView_whenLoadEnds() {
        
        sut.loadAuctions()
        invokeAuctionsInteractorCompletionWithGenericError()
        
        XCTAssertTrue(spyView.stopLoadingCalled)
    }
    
    func test_loadAuctions_invokesDisplayError_onFailure() {
        
        sut.loadAuctions()
        invokeAuctionsInteractorCompletionWithGenericError()
        
        XCTAssertTrue(spyView.displayLoadingCalled)
        XCTAssertEqual(spyView.displayErrorTitle, "Whoops - Something went wrong")
        XCTAssertEqual(spyView.displayErrorDescription, "Please check your internet connection")
    }
    
    func test_loadAuctions_invokesDisplayAuctions_onSuccess() {
        
        let testAuction = makeTestAuction()
        sut.loadAuctions()
        
        invokeAuctionsInteractorCompletion(with: testAuction)
        
        let auctionViewModel = spyView.displayAuctionsArgument[0]
        XCTAssertTrue(spyView.displayAuctionsCalled)
        XCTAssertEqual(auctionViewModel.title, testAuction.title)
        XCTAssertEqual(auctionViewModel.rate, Formatter.percentFormatter.string(for: testAuction.rate))
        XCTAssertEqual(auctionViewModel.rating, testAuction.riskBand.rawValue)
        XCTAssertEqual(auctionViewModel.closeDate, Formatter.dateFormatter.string(from: testAuction.closeDate))
    }
}

// MARK: Helpers
extension AuctionsListPresenterTests {
    func invokeAuctionsInteractorCompletionWithGenericError() {
        mockAuctionsInteractor.latestLoadAuctionsCompletion?(Result<[Auction]>.failure(ServiceError.genericError))
    }
    
    func invokeAuctionsInteractorCompletion(with auction: Auction) {
        mockAuctionsInteractor.latestLoadAuctionsCompletion?(Result<[Auction]>.success([auction]))
    }
    
    func makeTestAuction() -> Auction {
        return Auction(title: "Test auction",
                       rate: 0.08,
                       estimatedReturn: Money(cents: 10),
                       riskBand: RiskBand.aPlus,
                       closeDate: Date())
    }
}
