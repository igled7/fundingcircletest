//
//  AuctionsListViewController.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit

final class AuctionsListViewController: UIViewController {
    
    private var loadingViewController: LoadingViewController?
    private var errorViewController: ErrorViewController?
    private var tableViewController: AuctionsTableViewController?
    
    let presenter: AuctionsListPresenter
    init(presenter: AuctionsListPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        self.presenter.auctionsListView = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "FC Test"
        presenter.loadAuctions()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: AuctionsListView
extension AuctionsListViewController: AuctionsListView {
    
    func displayLoading() {
        guard loadingViewController == nil else { return }
        let viewController = LoadingViewController()
        add(viewController)
        loadingViewController = viewController
    }
    
    func stopLoading() {
        loadingViewController?.remove()
        loadingViewController = nil
    }
    
    func displayError(title: String, description: String) {
        errorViewController?.remove()
        let errorVC = ErrorViewController.instantiateViewControllerFromStoryBoard()
        errorVC.delegate = self
        errorViewController = errorVC
        add(errorVC)
        errorVC.displayError(withTitle: title, description: description)
    }
    
    func displayAuctions(_ auctions: [AuctionViewModel]) {
    
        let viewController: AuctionsTableViewController
        if let tableVC = tableViewController {
            viewController = tableVC
        } else {
            viewController = AuctionsTableViewController.make(withDelegate: self)
            tableViewController = viewController
            add(viewController)
        }
        viewController.displayAuctions(auctions)
    }
}

// MARK: AuctionsListView
extension AuctionsListViewController: AuctionsTableViewControllerDelegate {

    func auctionsTableViewController(_ sender: AuctionsTableViewController, didSelect auction: AuctionViewModel) {
        presenter.presentDetails(for: auction)
    }
}

// MARK: ErrorViewControllerDelegate
extension AuctionsListViewController: ErrorViewControllerDelegate {
    func errorViewControllerDidSelectRetry(_ sender: ErrorViewController) {
        errorViewController?.remove()
        errorViewController = nil
        presenter.loadAuctions()
    }
}

private extension AuctionsTableViewController {
    static func make(withDelegate delegate: AuctionsTableViewControllerDelegate) -> AuctionsTableViewController {
        let viewController = AuctionsTableViewController()
        viewController.delegate = delegate
        return viewController
    }
}
