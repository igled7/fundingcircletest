//
//  ErrorViewController.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit

protocol ErrorViewControllerDelegate: AnyObject {
    func errorViewControllerDidSelectRetry(_ sender: ErrorViewController)
}

final class ErrorViewController: UIViewController, StoryBoardLoadableViewController {
    
    weak var delegate: ErrorViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func displayError(withTitle title: String, description: String) {
        titleLabel.text = title
        descriptionLabel.text = description
    }
    
    @IBAction func onRetryTap(_ sender: UIButton) {
        delegate?.errorViewControllerDidSelectRetry(self)
    }
}
