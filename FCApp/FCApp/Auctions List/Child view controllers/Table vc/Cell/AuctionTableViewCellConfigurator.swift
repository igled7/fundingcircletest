//
//  AuctionTableViewCellConfigurator.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation

final class AuctionTableViewCellConfigurator {
    
    static func configure(_ cell: AuctionTableViewCell, forDisplaying viewModel: AuctionViewModel) {
        cell.titleLabel.text = viewModel.title
        cell.rateLabel.text = "Average rate: \(viewModel.rate)"
        cell.ratingLabel.text = "Risk band: \(viewModel.rating)"
        cell.closeDateLabel.text = "Close date: \(viewModel.closeDate)"
    }
}
