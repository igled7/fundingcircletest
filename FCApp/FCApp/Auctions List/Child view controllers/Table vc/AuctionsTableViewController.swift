//
//  AuctionsTableViewController.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit

protocol AuctionsTableViewControllerDelegate: AnyObject {
    func auctionsTableViewController(_ sender: AuctionsTableViewController, didSelect auction: AuctionViewModel)
}

final class AuctionsTableViewController: UITableViewController {
    
    weak var delegate: AuctionsTableViewControllerDelegate?
    
    var auctions: [AuctionViewModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(AuctionTableViewCell.self)
    }
    
    func displayAuctions(_ auctions: [AuctionViewModel]) {
        self.auctions = auctions
    }
}

// MARK: UITableViewDataSource
extension AuctionsTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return auctions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as AuctionTableViewCell
        AuctionTableViewCellConfigurator.configure(cell, forDisplaying: auctions[indexPath.row])
        return cell
    }
}

// MARK: UITableViewDelegate
extension AuctionsTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.auctionsTableViewController(self, didSelect: auctions[indexPath.row])
    }
}
