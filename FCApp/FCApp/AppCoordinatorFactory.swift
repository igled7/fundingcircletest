//
//  AppCoordinatorFactory.swift
//  FCApp
//
//  Created by Daniel on 16/09/2018.
//  Copyright © 2018 Daniel. All rights reserved.
//

import Foundation
import FCInteractors

final class AppCoordinatorFactory {
    private let interactorsFactory: InteractorsFactory
    init(interactorsFactory: InteractorsFactory) {
        self.interactorsFactory = interactorsFactory
    }
    
    func makeAuctionListViewController(with routingDelegate: AuctionsListRoutingDelegate) -> UIViewController {
        let interactor = interactorsFactory.makeAuctionsInteractor()
        let presenter = AuctionsListPresenter(auctionsInteractor: interactor)
        let viewController = AuctionsListViewController(presenter: presenter)
        presenter.routingDelegate = routingDelegate
        return viewController
    }
    
    func makeAuctionDetailsViewController(withAuction auction: Auction) -> UIViewController {
        let presenter = AuctionDetailsPresenter(auction: auction)
        let viewController = AuctionDetailsViewController.instantiateViewControllerFromStoryBoard()
        viewController.initialize(withPresenter: presenter)
        return viewController
    }
}
