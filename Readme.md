**FC coding test**

I have created two Xcode projects.


1. Dynamic framework for the business logic. This framework contains an interactor for fetching the auctions from the API. It also has the logic to compute the estimated return for an auction.
2. The app. There are two use cases: One for showing the list of auctions and another for displaying the auction details.

**Intructions**

You need to open the workspace to be able to build and run the app. There is a shared schema for the app that you can use for running the tests in the dynamic framework and the app.

**Notes**

* I was focused on the architecture of the app. I wrote tests for the dynamic framework and I also have an example of how I would write tests for the presentation layer (AuctionsListPresenter).
* I didn't spend a lot of time in the UI.
* I haven't localized strings and errors. I use a generic harcoded string for all possible errors.
* The API was returning a risk band of C-. I have assumed that C- and C have the same estimated bad debt.
* I haven't used the field 'amount_cents' from the response in the formula 𝑒𝑟𝑎 = (1 + 𝑎𝑟 − 𝑒𝑏𝑑 − 𝑓) 𝑥 𝑏𝑎. I am assuming that ba is £20. I may have missed something in this one.


